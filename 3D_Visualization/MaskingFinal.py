#!/usr/bin/env python3

# A typical VTK pipeline as follows:
# - Sources ( from files using readers )
# - Filters ( apply filters, which could be cascaded one after the other )
# - Mappers ( transform data to geometry )
# - Actors  ( adjust graphics object's properties )
# - Renderers
# - Windows

import vtk
from vtk.util.colors import tomato
import nibabel as nib
import numpy as np

colors = vtk.vtkNamedColors()

colors.SetColor("MaskColor1", [173, 173, 214, 255]) ##Brain
colors.SetColor("MaskColor2", [200, 56, 90, 255])  ##Core
colors.SetColor("MaskColor3", [21, 128, 120, 255])  ##Edema
colors.SetColor("MaskColor4", [26, 72, 118, 255])  ##Enhancing

brainReader = nib.load('FLAIR.nii.gz')
brainMat  = brainReader.get_data()

tumor = nib.load('seg.nii.gz')
data = tumor.get_data()

mask1 = (data == 0).astype(np.int16)
mask2 = (data == 1).astype(np.int16)
mask3 = (data == 2).astype(np.int16)
mask4 = (data == 4).astype(np.int16)

brain1 = np.multiply(brainMat, mask1)
brain2 = np.multiply(brainMat, mask2)
brain3 = np.multiply(brainMat, mask3)
brain4 = np.multiply(brainMat, mask4)

margin = 500
min1 = brain1.min()
max1 = brain1.max()

acc1 = margin + max1

min2 = brain2.min() + acc1
max2 = brain2.max() + acc1

acc2 = margin + max2

min3 = brain3.min() + acc2
max3 = brain3.max() + acc2

acc3 = margin + max3

min4 = brain4.min() + acc3
max4 = brain4.max() + acc3

brainMask1 = brain1
brainMask2 = np.multiply((brain2 + acc1), mask2)
brainMask3 = np.multiply((brain3 + acc2), mask3)
brainMask4 = np.multiply((brain4 + acc3), mask4)

#brain = brainMask1 + brainMask2 + brainMask3 + brainMask4

brainBytes1  = brainMask1.tobytes()
brainDims1   = brainMask1.shape

brainImporter1 = vtk.vtkImageImport()
brainImporter1.CopyImportVoidPointer(brainBytes1, len(brainBytes1))
brainImporter1.SetDataScalarTypeToShort()
brainImporter1.SetNumberOfScalarComponents(1)
brainImporter1.SetWholeExtent(0,brainDims1[2]-1,0,brainDims1[1]-1,0,brainDims1[0]-1)
brainImporter1.SetDataExtentToWholeExtent()

brainBytes2  = brainMask2.tobytes()
brainDims2   = brainMask2.shape

brainImporter2 = vtk.vtkImageImport()
brainImporter2.CopyImportVoidPointer(brainBytes2, len(brainBytes2))
brainImporter2.SetDataScalarTypeToShort()
brainImporter2.SetNumberOfScalarComponents(1)
brainImporter2.SetWholeExtent(0,brainDims2[2]-1,0,brainDims2[1]-1,0,brainDims2[0]-1)
brainImporter2.SetDataExtentToWholeExtent()

brainBytes3  = brainMask3.tobytes()
brainDims3   = brainMask3.shape

brainImporter3 = vtk.vtkImageImport()
brainImporter3.CopyImportVoidPointer(brainBytes3, len(brainBytes3))
brainImporter3.SetDataScalarTypeToShort()
brainImporter3.SetNumberOfScalarComponents(1)
brainImporter3.SetWholeExtent(0,brainDims3[2]-1,0,brainDims3[1]-1,0,brainDims3[0]-1)
brainImporter3.SetDataExtentToWholeExtent()

brainBytes4  = brainMask4.tobytes()
brainDims4   = brainMask4.shape

brainImporter4 = vtk.vtkImageImport()
brainImporter4.CopyImportVoidPointer(brainBytes4, len(brainBytes4))
brainImporter4.SetDataScalarTypeToShort()
brainImporter4.SetNumberOfScalarComponents(1)
brainImporter4.SetWholeExtent(0,brainDims4[2]-1,0,brainDims4[1]-1,0,brainDims4[0]-1)
brainImporter4.SetDataExtentToWholeExtent()

#######################################################################################

brainExtractor1 = vtk.vtkMarchingCubes()
brainExtractor1.SetInputConnection(brainImporter1.GetOutputPort())
brainExtractor1.SetValue(0, 1)
    
brainStripper1 = vtk.vtkStripper()
brainStripper1.SetInputConnection(brainExtractor1.GetOutputPort())

brainExtractor2 = vtk.vtkMarchingCubes()
brainExtractor2.SetInputConnection(brainImporter2.GetOutputPort())
brainExtractor2.SetValue(0, 1)

brainStripper2 = vtk.vtkStripper()
brainStripper2.SetInputConnection(brainExtractor2.GetOutputPort())

brainExtractor3 = vtk.vtkMarchingCubes()
brainExtractor3.SetInputConnection(brainImporter3.GetOutputPort())
brainExtractor3.SetValue(0, 1)

brainStripper3 = vtk.vtkStripper()
brainStripper3.SetInputConnection(brainExtractor3.GetOutputPort())

brainExtractor4 = vtk.vtkMarchingCubes()
brainExtractor4.SetInputConnection(brainImporter4.GetOutputPort())
brainExtractor4.SetValue(0, 1)

brainStripper4 = vtk.vtkStripper()
brainStripper4.SetInputConnection(brainExtractor4.GetOutputPort())
#######################################################################################

brainMapper1 = vtk.vtkPolyDataMapper()
brainMapper1.SetInputConnection(brainStripper1.GetOutputPort())
brainMapper1.ScalarVisibilityOff()

brainMapper2 = vtk.vtkPolyDataMapper()
brainMapper2.SetInputConnection(brainStripper2.GetOutputPort())
brainMapper2.ScalarVisibilityOff()

brainMapper3 = vtk.vtkPolyDataMapper()
brainMapper3.SetInputConnection(brainStripper3.GetOutputPort())
brainMapper3.ScalarVisibilityOff()

brainMapper4 = vtk.vtkPolyDataMapper()
brainMapper4.SetInputConnection(brainStripper4.GetOutputPort())
brainMapper4.ScalarVisibilityOff()

#######################################################################################

brainActor1 = vtk.vtkActor()
brainActor1.SetMapper(brainMapper1)
brainActor1.GetProperty().SetDiffuseColor(colors.GetColor3d("MaskColor1"))
brainActor1.GetProperty().SetSpecular(.3)
brainActor1.GetProperty().SetSpecularPower(20)
brainActor1.GetProperty().SetOpacity(.2)

brainActor2 = vtk.vtkActor()
brainActor2.SetMapper(brainMapper2)
brainActor2.GetProperty().SetDiffuseColor(colors.GetColor3d("MaskColor2"))
brainActor2.GetProperty().SetSpecular(.3)
brainActor2.GetProperty().SetSpecularPower(20)
brainActor2.GetProperty().SetOpacity(.5)

brainActor3 = vtk.vtkActor()
brainActor3.SetMapper(brainMapper3)
brainActor3.GetProperty().SetDiffuseColor(colors.GetColor3d("MaskColor3"))
brainActor3.GetProperty().SetSpecular(.3)
brainActor3.GetProperty().SetSpecularPower(20)
brainActor3.GetProperty().SetOpacity(.1)

brainActor4 = vtk.vtkActor()
brainActor4.SetMapper(brainMapper4)
brainActor4.GetProperty().SetDiffuseColor(colors.GetColor3d("MaskColor4"))
brainActor4.GetProperty().SetSpecular(.3)
brainActor4.GetProperty().SetSpecularPower(20)
brainActor4.GetProperty().SetOpacity(.2)

#######################################################################################

camera = vtk.vtkCamera()
camera.SetViewUp(0, 0, -1)
camera.SetPosition(0, -1, 0)
camera.SetFocalPoint(0, 0, 0)
camera.ComputeViewPlaneNormal()
camera.Azimuth(30.0)
camera.Elevation(30.0)

ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

ren.SetBackground(255,255,255)
renWin.SetSize(800, 600)

ren.AddActor(brainActor1)
ren.AddActor(brainActor2)
ren.AddActor(brainActor3)
ren.AddActor(brainActor4)
ren.SetActiveCamera(camera)
ren.ResetCamera()
camera.Dolly(1.5)

#def RotateX(obj, event):
#    # print(brainVolume.GetOrientation())
#    # print(tumorVolume.GetOrientation())
#    t1 = time.time()
#    camera.Roll(5.0)
#    camera.Pitch(5.0)
#    camera.Yaw(5.0)
#    camera.Azimuth(1)
#    camera.Elevation(1)
#    t2 = time.time()
#    renWin.Render()
#    t3 = time.time()
#    print("Rotating  in {} ms".format(1000*(t2 - t1)))
#    print("Rendering in {} ms".format(1000*(t3 - t2)))
#def RotateY(obj, event):
#    print(brainVolume.GetOrientation())
#    print(tumorVolume.GetOrientation())
#    brainVolume.RotateY(1.0)
#    tumorVolume.RotateY(1.0)
#    renWin.Render()
#def RotateZ(obj, event):
#    print(brainVolume.GetOrientation())
#    print(tumorVolume.GetOrientation())
#    brainVolume.RotateZ(1.0)
#    tumorVolume.RotateZ(1.0)
#    renWin.Render()
#
#events = []
#
#def Keypress(obj, event):
#    key = obj.GetKeySym()
#    if key == "1":
#        print "I am a one"
#        events.append(iren.AddObserver("TimerEvent", RotateX))
#    elif key == "2":
#        events.append(iren.AddObserver("TimerEvent", RotateY))
#    elif key == "3":
#        events.append(iren.AddObserver("TimerEvent", RotateZ))
#    elif key == "4":
#        for e in events:
#            iren.RemoveObserver(e)
#
## iren.SetInteractorStyle(None)
#iren.AddObserver("KeyPressEvent", Keypress)
#
#
## Interact with the data.

iren.Initialize()
#iren.CreateRepeatingTimer(16)
renWin.Render()
iren.Start()

