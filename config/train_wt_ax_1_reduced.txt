[data]
data_root             = /home/malaa/Datasets/MICCAI_BraTS17_Data_Training/B17
data_names            = config/train_names_all_reduced.txt
modality_postfix      = [flair, t1, t1ce, t2]
label_postfix         = seg
file_postfix          = nii.gz
with_ground_truth     = True
batch_size            = 1
data_shape            = [19, 144, 144, 4]
label_shape           = [11, 144, 144, 1]
label_convert_source  = [0, 1, 2, 4]
label_convert_target  = [0, 1, 1, 1]
batch_slice_direction = axial
train_with_roi_patch  = False
label_roi_mask        = 
roi_patch_margin      = 

[network]
net_type            = TF_MSNet
net_name            = TF_MSNet_WT32_1_reduced
downsample_twice    = True
class_num           = 2

[training]
learning_rate      = 1e-3
decay              = 1e-7
maximal_iteration  = 10000
snapshot_iteration = 3
start_iteration    = 0
test_iteration     = 2
test_step          = 10
model_pre_trained  = 
model_save_prefix  = tf_msnet_wt32_1_reduced

[ext]
type = 0
renorm = True
data_format = channels_last
