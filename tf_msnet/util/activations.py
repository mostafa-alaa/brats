from __future__ import absolute_import
import tensorflow as tf
import tf_msnet.util.globals as g

def prelu(alpha_initializer=0.0, alpha_regularizer=None, data_format=None):
    i = g.get_channel_axis(data_format)
    def _prelu(features, name=None):
        shape = [1] * features.shape.ndims
        shape[i] = features.shape[i]
        with tf.variable_scope(name, "prelu"):
            initializer = tf.constant(alpha_initializer, features.dtype, shape)
            alpha = tf.get_variable("alpha",
                                    initializer=initializer,
                                    regularizer=alpha_regularizer)
            return tf.nn.relu(features) - alpha * tf.nn.relu(-features)
    return _prelu

def swish(features, name=None):
    with tf.name_scope(name, "swish"):
        return features * tf.sigmoid(features)
