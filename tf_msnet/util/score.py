from __future__ import division
import numpy as np
from scipy import ndimage

def compute_score(p, y, w, axis):
    p = p.argmax(axis)
    y = y.squeeze(axis)
    w = w.squeeze(axis)

    p = np.bitwise_and(p, w)
    s = ndimage.generate_binary_structure(3, 2)
    p = np.asarray([ndimage.morphology.binary_closing(v, s) for v in p])
    return np_score(p, y)

def np_score(p, y):
    """
        Computes the Dice, Sensitivity, Specificity, Accuracy
        between prediction 'p' and ground truth 'y'.

        Args:
            p: array_like.
                Must be one of the following types:
                    bool,
                    int8,  int16,  int32,  int64,
                    uint8, uint16, uint32, uint64.
                Must have the following shape:
                    [batch, spatial dimensions ...].
                Must have { False, True } or { 0, 1 } values only.
            y: array_like. Must follow the same constraints as 'p'.

        Returns:
            A dict of Dice, Sensitivity, Specificity, Accuracy.
    """
    axes  = tuple(range(1, y.ndim))
    total = np.prod(y.shape[1:])

    false = np.bitwise_xor(p,y).sum(axes)
    true  = total - false

    tpos  = np.bitwise_and(p,y).sum(axes)
    tneg  = true - tpos

    ypos  = y.sum(axes)
    yneg  = total - ypos

    ppos  = p.sum(axes)
#   pneg  = total - ppos

    yposnz = ypos != 0

    dice = ((2 * tpos[yposnz]) / (ppos[yposnz] + ypos[yposnz])).mean()
    sens = (tpos[yposnz] / ypos[yposnz]).mean()
    spec = (tneg / yneg).mean()
    accu = (true / total).mean()

    return {
        "Dice": dice,
        "Sensitivity": sens,
        "Specificity": spec,
        "Accuracy": accu,
    }
