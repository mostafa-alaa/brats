from __future__ import absolute_import
import numpy as np
import tensorflow as tf
import tf_msnet.util.globals as g

def he_normal(shape, dtype=g._FLOATX, partition_info=None, seed=None):
    """ Initialize filters Tensors of shape
            [spatial_filter_shape[0],
            ...,
            spatial_filter_shape[N-1],
            num_input_channels,
            num_output_channels]
    """
    assert partition_info is None
    stddev = np.sqrt(2. / np.prod(shape[:-1]))
    return tf.truncated_normal(shape, 0.0, stddev, dtype, seed, "he_normal")
