import six
import itertools
import tensorflow as tf

def device_setter(ps_device, worker_device):
    ps_ops = ['Variable', 'VariableV2', 'VarHandleOp']
    def _device_chooser(op):
        current_device = tf.DeviceSpec.from_string(op.device or "")
        node_def = op if isinstance(op, tf.NodeDef) else op.node_def
        device = ps_device if node_def.op in ps_ops else worker_device
        device_spec = tf.DeviceSpec.from_string(device)
        device_spec.merge_from(current_device)
        return device_spec.to_string()
    return _device_chooser

def get_tf_msnet_gpus(x, y, w, num_gpus, model_fn, loss_fn, *args, **kwargs):
    with tf.variable_scope('Split'):
        with tf.device('/cpu:0'):
            x = tf.split(x, num_gpus, 0)
            y = tf.split(y, num_gpus, 0)
            w = tf.split(w, num_gpus, 0)

    tower_preds    = []
    tower_losses   = []
    tower_gradvars = []
    with tf.variable_scope('Vars', reuse=tf.AUTO_REUSE), tf.name_scope('Ops/'):
        for i in range(num_gpus):
            with tf.name_scope('Tower'):
                with tf.device(device_setter('/cpu:0', '/gpu:{}'.format(i))):
                    pred = model_fn(x[i], *args, **kwargs)
                    loss = loss_fn(pred, y[i], w[i])
                    vars = tf.trainable_variables()
                    grad = tf.gradients(loss, vars)
                    tower_preds.append(pred)
                    tower_losses.append(loss)
                    tower_gradvars.append(zip(grad, vars))

    with tf.variable_scope('Merge'):
        with tf.device('/cpu:0'):
            pred = tf.concat(tower_preds, 0)
            loss = tf.reduce_mean(tower_losses)

        gradvars = []
        with tf.name_scope('Gradient_Averaging'):
            all_grads = {}
            for grad, var in itertools.chain(*tower_gradvars):
                if grad is not None:
                    all_grads.setdefault(var, []).append(grad)
            for var, grads in six.iteritems(all_grads):
                with tf.device(var.device):
                    avg_grad = grads[0] if len(grads) == 1 \
                    else tf.multiply(tf.add_n(grads), 1. / len(grads))
                gradvars.append((avg_grad, var))

    return pred, loss, gradvars
