from __future__ import absolute_import
import tensorflow as tf
import numpy as np
import tf_msnet.util.globals as g
from tensorflow.contrib.layers import l2_regularizer
from tf_msnet.util.initializers import he_normal
from tf_msnet.util.activations import prelu
from tf_msnet.util.layers import ResBlock, ConvBlock, DeconvBlock, ZeroPaddingAxis

def lin_interp(M):
    dx = M[1][0] - M[0][0]
    m = (M[1][1] - M[0][1]) / dx
    c = (M[1][0]*M[0][1] - M[0][0]*M[1][1]) / dx
    return m, c

class TF_MSNet(object):

    def __init__(self,
                 num_classes=2,
                 kern_initializer=he_normal,
                 kern_regularizer=l2_regularizer(1e-7),
                 bias_initializer=None,
                 bias_regularizer=None,
                 renorm=True,
                 activation=None,
                 data_format=g._DATA_FORMAT,
                 name=None):

        self.num_classes = num_classes
        self.kern_initializer = kern_initializer
        self.kern_regularizer = kern_regularizer
        self.bias_initializer = bias_initializer
        self.bias_regularizer = bias_regularizer
        self.renorm_clipping = None
        self.activation = activation or prelu(0.0, kern_regularizer, data_format)
        self.data_format = data_format
        self.name = name

        if renorm == True:
            renorm = {'rmax': [[5e3,1],[4e4,3]], 'dmax': [[5e3,0],[25e3,5]]}
        if renorm:
            def _get_renorm_clipping(dtype=g._FLOATX):
                clip = {}
                global_step = tf.train.get_or_create_global_step()
                with tf.name_scope(None, "renorm_clipping"):
                    global_step = tf.cast(global_step, dtype)
                    for k in renorm:
                        M = renorm[k]
                        m, c = lin_interp(M)
                        y = m*global_step + c
                        clip[k] = tf.clip_by_value(y, M[0][1], M[1][1])
                    if 'rmin' not in renorm and 'rmax' in renorm:
                        clip['rmin'] = tf.reciprocal(clip['rmax'])
                return clip
            self.renorm_clipping = _get_renorm_clipping

    def __call__(self, inputs, type, training=False):
        """
            Parameters:
                inputs: input tensor(s).
                type: 0, 1, 2 for WNet, TNet, ENet respectively.
        """

        downsample_twice = type < 2
        renorm = training and self.renorm_clipping is not None
        renorm_clipping = renorm and self.renorm_clipping(inputs.dtype)
        name = self.name or {
            0: 'MSNet_WNet',
            1: 'MSNet_TNet',
            2: 'MSNet_ENet'
        }[type]

        LResBlock = lambda *args, **kwargs: ResBlock(*args, **kwargs,
            kernel_initializer=self.kern_initializer,
            kernel_regularizer=self.kern_regularizer,
            renorm=renorm,
            renorm_clipping=renorm_clipping,
            activation=self.activation,
            data_format=self.data_format
        )

        LFuseBlock = lambda *args, **kwargs: ConvBlock(*args, **kwargs,
            padding='valid',
            kernel_initializer=self.kern_initializer,
            kernel_regularizer=self.kern_regularizer,
            renorm=renorm,
            renorm_clipping=renorm_clipping,
            activation=self.activation,
            data_format=self.data_format
        )

        LPoolBlock = lambda *args, **kwargs: ConvBlock(*args, **kwargs,
            padding='same',
            kernel_initializer=self.kern_initializer,
            kernel_regularizer=self.kern_regularizer,
            renorm=renorm,
            renorm_clipping=renorm_clipping,
            activation=self.activation,
            data_format=self.data_format
        )

        LSliceBlock = lambda inputs, margin, name: tf.slice(inputs,
            np.array(margin, np.int32),
            (inputs.shape.as_list() - 2*np.array(margin)).astype(np.int32),
            name=name
        )

        LDeconvBlock = lambda *args, **kwargs: DeconvBlock(*args, **kwargs,
            padding='same',
            kernel_initializer=self.kern_initializer,
            kernel_regularizer=self.kern_regularizer,
            renorm=renorm,
            renorm_clipping=renorm_clipping,
            activation=self.activation,
            data_format=self.data_format
        )

        axis = g.get_channel_axis(self.data_format)

        with tf.variable_scope(name):
            block1_1 = LResBlock(32, (1,3,3),
                res_fn = ZeroPaddingAxis(32, axis),
                name = 'block1_1'
            )

            block1_2 = LResBlock(32, (1,3,3),
                name = 'block1_2'
            )

            block2_1 = LResBlock(32, (1,3,3),
                name = 'block2_1'
            )

            block2_2 = LResBlock(32, (1,3,3),
                name = 'block2_2'
            )

            block3_1 = LResBlock(32, (1,3,3),
                dilation_rate = (1,1,1),
                name = 'block3_1'
            )

            block3_2 = LResBlock(32, (1,3,3),
                dilation_rate = (1,2,2),
                name = 'block3_2'
            )

            block3_3 = LResBlock(32, (1,3,3),
                dilation_rate = (1,3,3),
                name = 'block3_3'
            )

            block4_1 = LResBlock(32, (1,3,3),
                dilation_rate = (1,3,3),
                name = 'block4_1'
            )

            block4_2 = LResBlock(32, (1,3,3),
                dilation_rate = (1,2,2),
                name = 'block4_2'
            )

            block4_3 = LResBlock(32, (1,3,3),
                dilation_rate = (1,1,1),
                name = 'block4_3'
            )

            fuse1 = LFuseBlock(32, (3,1,1), name='fuse1')
            fuse2 = LFuseBlock(32, (3,1,1), name='fuse2')
            fuse3 = LFuseBlock(32, (3,1,1), name='fuse3')
            fuse4 = LFuseBlock(32, (3,1,1), name='fuse4')

            pool1 = LPoolBlock(32, (1,3,3),
                strides = (1,2,2),
                name = 'pool1'
            )

            pool2 = LPoolBlock(32, (1,3,3),
                strides = (1,2,2),
                name = 'pool2'
            )

            conv1_1 = ConvBlock(self.num_classes, (1,3,3),
                padding='same',
                kernel_initializer = self.kern_initializer,
                kernel_regularizer = self.kern_regularizer,
                with_bn = False,
                activation = None,
                data_format = self.data_format,
                name = 'conv1_1'
            )

            deconv1_1 = LDeconvBlock(self.num_classes*1, (1,3,3),
                strides = (1,2,2),
                name = 'deconv1_1'
            )

            deconv2_1 = LDeconvBlock(self.num_classes*2, (1,3,3),
                strides = (1,2,2),
                name = 'deconv2_1'
            )

            deconv2_2 = LDeconvBlock(self.num_classes*2, (1,3,3),
                strides = (1,2,2),
                name = 'deconv2_2'
            )

            deconv3_1 = LDeconvBlock(self.num_classes*4, (1,3,3),
                strides = (1,2,2),
                name = 'deconv3_1'
            )

            deconv3_2 = LDeconvBlock(self.num_classes*4, (1,3,3),
                strides = (1,2,2),
                name = 'deconv3_2'
            )

            pred = ConvBlock(self.num_classes, (1,3,3),
                padding='same',
                kernel_initializer = self.kern_initializer,
                kernel_regularizer = self.kern_regularizer,
                with_bn = False,
                activation = None,
                data_format = self.data_format,
                name = 'pred'
            )

            o1 = inputs
            o1 = block1_1(o1, training=training)
            o1 = block1_2(o1, training=training)
            o1 = fuse1(o1, training=training)
            o1 = pool1(o1, training=training) if downsample_twice else o1
            o1 = block2_1(o1, training=training)
            o1 = block2_2(o1, training=training)
            o1 = fuse2(o1, training=training)

            o2 = o1
            o2 = pool2(o2, training=training)
            o2 = block3_1(o2, training=training)
            o2 = block3_2(o2, training=training)
            o2 = block3_3(o2, training=training)
            o2 = fuse3(o2, training=training)

            o3 = o2
            o3 = block4_1(o3, training=training)
            o3 = block4_2(o3, training=training)
            o3 = block4_3(o3, training=training)
            o3 = fuse4(o3, training=training)

            o1 = LSliceBlock(o1, axis == 1 and [0,0,2,0,0] or [0,2,0,0,0], 'extract1')
            o2 = LSliceBlock(o2, axis == 1 and [0,0,1,0,0] or [0,1,0,0,0], 'extract2')
            # o3 = LSliceBlock(o3, axis == 1 and [0,0,0,0,0] or [0,0,0,0,0], 'extract3')

            o1 = deconv1_1(o1, training=training) if downsample_twice \
            else conv1_1(o1, training=training)
            o2 = deconv2_1(o2, training=training)
            o2 = deconv2_2(o2, training=training) if downsample_twice else o2
            o3 = deconv3_1(o3, training=training)
            o3 = deconv3_2(o3, training=training) if downsample_twice else o3

            o = tf.concat([o1, o2, o3], axis=axis)
            o = pred(o)
            return o
