#!/usr/bin/env python3

import os
import sys
import signal
import webbrowser
from contextlib import redirect_stderr
from multiprocessing import Process
from tensorflow import app

def launch_tb(graph_dir, port):
    def _f(r, w):
        with os.fdopen(w, 'w') as w, redirect_stderr(w):
            try:
                from tensorboard import program
                server = program.TensorBoard()
                server.configure(["--logdir", graph_dir, "--port", port])
                app.run(server.main)
            except ImportError:
                from tensorboard import main
                app.run(main.main, [None, "--logdir", graph_dir, "--port", port])

    r, w = os.pipe()
    p = Process(target=_f, args=(r, w))
    p.start()
    os.close(w)
    with os.fdopen(r) as r:
        r.readline()
    return p

if __name__ == "__main__":
    if len(sys.argv) == 2:
        port = "6006"
    elif len(sys.argv) == 3:
        port = sys.argv[2]
    else:
        print("python {} GRAPH_DIR [PORT]".format(sys.argv[0]))
        sys.exit(1)

    p = launch_tb(sys.argv[1], port)
    webbrowser.open_new_tab("http://127.0.0.1:" + port)
    input("Press Enter to quit")
    os.kill(p.pid, signal.SIGINT)
